# About the project

**Quest with notifications** is an updated version of [Quest](https://bitbucket.org/catness/quest) app for the previous codelab series, with a new option of notifications, following [Advanced Android in Kotlin - Using Notifications](https://codelabs.developers.google.com/codelabs/advanced-android-kotlin-training-notifications). There's no new functionality, except that upon completion of a quest, a new quest doesn't appear immediately but after a certain time period, and notification is sent to inform the user about it. (Potential use: to avoid overwhelming the user with quests, to prevent a burnout.)

Instead of Alarm Manager/Broadcast Receiver, presented in the codelab, I decided to use Work Manager, because apparently it's not recommended to access the database from Broadcast Receiver, and I needed to activate a new quest and to send its ID to the user in notification (for the action button). Broadcast Receiver is still used, but just for testing. Also for testing, the interval for creating a new quest is currently 1 minute. (It should be 1 hour or several hours for production.)

Another option not covered in the codelab: passing parameters to fragments opened from the notification. I'm using NavDeepLinkBuilder.

The sandwich image used in notification is by [Clker-Free-Vector-Images from Pixabay](https://pixabay.com/vectors/sandwich-food-sausage-snack-fresh-23473/).
___

My blog: [Cat's Mysterious Box](http://catness.org/logbook/)
