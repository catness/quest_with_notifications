package org.catness.questnotif.inventory

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import org.catness.questnotif.database.Reward

@BindingAdapter("rewardImage")
fun ImageView.setRewardImage(item: Reward) {
    // item.name
        var uri: String = "@drawable/" + item.name
        var imageResource: Int = context.resources.getIdentifier(uri, null, context.packageName)
        setImageResource(imageResource)
}

@BindingAdapter("rewardAmount")
fun TextView.setRewardAmount(item: Reward) {
        text = item.amount.toString()
}


