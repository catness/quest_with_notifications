package org.catness.questnotif.inventory

import android.app.Application
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import org.catness.questnotif.database.QuestDatabaseDao
import org.catness.questnotif.formatRewards

class InventoryViewModel(
        dataSource: QuestDatabaseDao,
        application: Application
) : ViewModel() {
    val database = dataSource
    val rewards = database.getRewards()

    /**
     * Converted quests to Spanned for displaying.
     */
    val rewardsString = Transformations.map(rewards) { rewards ->
        formatRewards(rewards, application.resources)
    }

}