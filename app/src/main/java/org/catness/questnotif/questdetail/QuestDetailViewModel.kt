package org.catness.questnotif.questdetail

import android.app.AlarmManager
import android.app.Application
import android.app.PendingIntent
import android.app.PendingIntent.*
import android.content.Context
import android.content.Intent
import android.os.SystemClock
import android.util.Log
import androidx.core.app.AlarmManagerCompat
import androidx.lifecycle.*
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequest
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import kotlinx.coroutines.launch
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import org.catness.questnotif.database.Journal
import org.catness.questnotif.database.Quest
import org.catness.questnotif.database.QuestDatabaseDao
import org.catness.questnotif.receiver.AlarmReceiver
import org.catness.questnotif.work.QuestWorker
import org.catness.questnotif.work.QuestWorker.Companion.WORK_NEW_QUEST
import java.util.concurrent.TimeUnit

class QuestDetailViewModel(
    private val app: Application,
    private val questKey: Long = 0L,
    dataSource: QuestDatabaseDao
) : AndroidViewModel(app) {
    private val LOG_TAG: String = this.javaClass.simpleName

    private val alarmManager = app.getSystemService(Context.ALARM_SERVICE) as AlarmManager
    private val questIntent = Intent(app, AlarmReceiver::class.java)
    private val questPendingIntent: PendingIntent
    private val REQUEST_CODE = 0
    private val minute: Long = 60_000L
    private val second: Long = 1_000L
    val database = dataSource

    /** Coroutine variables */

    /**
     * viewModelJob allows us to cancel all coroutines started by this ViewModel.
     */
    private var viewModelJob = Job()

    /**
     * A [CoroutineScope] keeps track of all coroutines started by this ViewModel.
     *
     * Because we pass it [viewModelJob], any coroutine started in this uiScope can be cancelled
     * by calling `viewModelJob.cancel()`
     *
     * By default, all coroutines started in uiScope will launch in [Dispatchers.Main] which is
     * the main thread on Android. This is a sensible default because most coroutines started by
     * a [ViewModel] update the UI after performing some processing.
     */
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private val quest: LiveData<Quest> = database.getQuest(questKey)
    fun getQuest() = quest
    private val _navigateToQuestsFragment = MutableLiveData<Boolean?>()
    val navigateToQuestsFragment: LiveData<Boolean?>
        get() = _navigateToQuestsFragment

    fun doneNavigating() {
        _navigateToQuestsFragment.value = null
    }

    private var _showSnackbarEvent = MutableLiveData<String>()
    val showSnackBarEvent: LiveData<String>
        get() = _showSnackbarEvent


    init {
        // questPendingIntent is just for debugging, to try out broadcast
        questPendingIntent = PendingIntent.getBroadcast(
            getApplication(),
            REQUEST_CODE,
            questIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    fun onCompleted() {
        val triggerTime = SystemClock.elapsedRealtime() + second * 5
        // show a toast after N seconds, just to try it out
        AlarmManagerCompat.setExactAndAllowWhileIdle(
            alarmManager,
            AlarmManager.ELAPSED_REALTIME_WAKEUP,
            triggerTime,
            questPendingIntent
        )

        viewModelScope.launch {
            val journal = Journal()
            journal.name = quest.value!!.name
            journal.description = quest.value!!.description

            val reward = database.getRandomReward()
            journal.reward = reward.description
            journal.reward_name = reward.name
            database.insertJournal(journal)
            database.incrementReward(reward.id)
            database.completeQuest(quest.value!!.id)
            val count: Long = database.countAvailableQuests()
            if (count == 0L) {
                database.resetAllQuests()
                Log.i(LOG_TAG, "Reset all quests")
            }

            // delay should be more like 1 hour, but minutes are better for debugging
             val request = OneTimeWorkRequestBuilder<QuestWorker>().
                setInitialDelay(1,TimeUnit.MINUTES).build()

            WorkManager.getInstance(getApplication()).enqueueUniqueWork(WORK_NEW_QUEST,ExistingWorkPolicy.KEEP,request)
           // database.activateRandomQuest() // this is in QuestWorker now, after delay
            _showSnackbarEvent.value = reward.description
        }
    }

    fun doneShowingSnackbar() {
        _showSnackbarEvent.value = null
        _navigateToQuestsFragment.value = true

    }

    /**
     * Called when the ViewModel is dismantled.
     * At this point, we want to cancel all coroutines;
     * otherwise we end up with processes that have nowhere to return to
     * using memory and resources.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }


}