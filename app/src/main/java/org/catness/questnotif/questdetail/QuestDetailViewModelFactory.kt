package org.catness.questnotif.questdetail

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import org.catness.questnotif.database.QuestDatabaseDao

class QuestDetailViewModelFactory(
    private val app: Application,
    private val questKey: Long,
    private val dataSource: QuestDatabaseDao
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(QuestDetailViewModel::class.java)) {
            return QuestDetailViewModel(app, questKey, dataSource) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}