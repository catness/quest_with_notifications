package org.catness.questnotif.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import org.catness.questnotif.R
import org.catness.questnotif.database.QuestDatabase

class AlarmReceiver : BroadcastReceiver() {
    private val LOG_TAG: String = this.javaClass.simpleName
    override fun onReceive(context: Context, intent: Intent) {
        Toast.makeText(context, context.getText(R.string.test), Toast.LENGTH_SHORT).show()
        // val dataSource = QuestDatabase.getInstance(context).questDatabaseDao
        // apparently it's not recommended to access database in BroadcastReceiver
        // we do it in QuestWorker instead
    }
}