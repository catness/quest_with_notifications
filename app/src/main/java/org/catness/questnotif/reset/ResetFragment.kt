package org.catness.questnotif

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import org.catness.questnotif.database.QuestDatabase
import org.catness.questnotif.databinding.FragmentResetBinding
import org.catness.questnotif.reset.ResetViewModel
import org.catness.questnotif.reset.ResetViewModelFactory
import com.google.android.material.snackbar.Snackbar

class ResetFragment : Fragment() {
 
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val binding: FragmentResetBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_reset, container, false
        )

        val application = requireNotNull(this.activity).application

        // Create an instance of the ViewModel Factory.
        val dataSource = QuestDatabase.getInstance(application).questDatabaseDao
        val viewModelFactory =
            ResetViewModelFactory(dataSource)

        // Get a reference to the ViewModel associated with this fragment.
        val resetViewModel =
            ViewModelProvider(
                this, viewModelFactory
            ).get(ResetViewModel::class.java)

        // To use the View Model with data binding, you have to explicitly
        // give the binding object a reference to it.
        binding.resetViewModel = resetViewModel

        binding.setLifecycleOwner(this)

        resetViewModel.navigateToQuestsFragment.observe(viewLifecycleOwner, Observer {
            if (it == true) { // Observed state is true.
                this.findNavController().navigate(
                    ResetFragmentDirections.actionResetFragmentToQuestsFragment()
                )
                // Reset state to make sure we only navigate once, even if the device
                // has a configuration change.
                resetViewModel.doneNavigating()
            }
        })

        resetViewModel.showSnackBarEvent.observe(viewLifecycleOwner, Observer {
           if (it == true) { // Observed state is true.
               Snackbar.make(
                       activity!!.findViewById(android.R.id.content),
                       getString(R.string.reset_message),
                       Snackbar.LENGTH_SHORT // How long to display the message.
               ).show()
               resetViewModel.doneShowingSnackbar()
           }
        })
        (activity as MainActivity).supportActionBar?.title = getString(R.string.reset)
        return binding.root

    }


}