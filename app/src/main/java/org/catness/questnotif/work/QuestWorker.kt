package org.catness.questnotif.work

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.navigation.NavDeepLinkBuilder
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import org.catness.questnotif.MainActivity
import org.catness.questnotif.R
import org.catness.questnotif.database.QuestDatabase
import org.catness.questnotif.questdetail.QuestDetailFragmentArgs


class QuestWorker(private val context: Context, params: WorkerParameters) :
    CoroutineWorker(context, params) {
    private val LOG_TAG: String = this.javaClass.simpleName
    private val NOTIFICATION_ID = 0

    companion object {
        const val WORK_NEW_QUEST = "org.catness.questnotif.work.NewQuest"
    }

    override suspend fun doWork(): Result {
        Log.i(LOG_TAG, "work start")
        val database = QuestDatabase.getInstance(applicationContext).questDatabaseDao
        val count = database.countActiveQuests()
        if (count < 3) {
            for (i in count..2) database.activateRandomQuest(System.currentTimeMillis())
            val questKey = database.getNewestQuestID()
            createNotification(
                context.getString(R.string.new_quest_title),
                context.getString(R.string.new_quest_message),
                questKey
            )
        }
        return Result.success()
    }

    fun createNotification(title: String, description: String, questKey: Long) {

        var notificationManager =
            applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // regular pending intent (upon clicking the notification)
        val contentIntent = Intent(applicationContext, MainActivity::class.java)
        val contentPendingIntent = PendingIntent.getActivity(
            applicationContext,
            NOTIFICATION_ID,
            contentIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        val args = QuestDetailFragmentArgs.Builder(questKey)
            .build()
            .toBundle()

        //https://stackoverflow.com/questions/52877277/send-argument-through-pendingintent-of-navdeeplinkbuilder
        // another pending intent for "notification action", going directly to specific fragment
        // it requires a paremeter, same as we pass to this fragment when we go from the main fragment

        val questIntent = NavDeepLinkBuilder(context)
            .setComponentName(MainActivity::class.java)
            .setGraph(R.navigation.navigation)
            .setDestination(R.id.questDetailFragment)
            .setArguments(args)
            .createPendingIntent()

        // this has to be a high quality image
        val questImage = BitmapFactory.decodeResource(
            applicationContext.resources,
            R.drawable.big_sandwich
        )
        val bigPicStyle = NotificationCompat.BigPictureStyle()
            .bigPicture(questImage)
            .bigLargeIcon(null)


        val notificationBuilder = NotificationCompat.Builder(
            applicationContext,
            applicationContext.getString(R.string.quest_notification_channel_id)
        )
            .setContentTitle(title)
            .setContentText(description)
            .setSmallIcon(R.drawable.slice_of_pizza)
            .setContentIntent(contentPendingIntent)
            .setAutoCancel(true)
            .setStyle(bigPicStyle)
            .setLargeIcon(questImage)
            .addAction(
                R.drawable.sandwich,
                applicationContext.getString(R.string.try_it),
                questIntent
            )

        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build())
    }

}