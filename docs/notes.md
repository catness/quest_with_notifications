Examples are from RecyclerViewClickHandler app (RVCH for short) unless noted otherwise

# Navigation anim effects

res/anim/slide_in_right.xml - used in res/anim/navigation/navigation.xml

included as additional parameters for _action_

app:enterAnim="@anim/slide_in_right"
app:popEnterAnim="@anim/slide_in_right"

_enter_ is A->B and _popEnter_ is B->A

# Adding stuff to gradle scripts

(For the project created from scratch)

* build.gradle (Project):

1. Instead of ext.kotlin_version, make the whole block of _ext_:

	ext {
        kotlin_version = '1.3.72'
        archLifecycleVersion = '1.1.1'
        room_version = '2.2.5'
        coroutine_version = '1.3.7'
        gradleVersion = '4.0.1'
        // navigationVersion = '1.0.0-alpha07' // it was in the example
        navigationVersion = '2.3.0'
        dataBindingCompilerVersion = gradleVersion // Always need to be the same.
	}

(The versions will probably change over time.)

2. Add one entry to _dependencies_ section:

	// classpath "android.arch.navigation:navigation-safe-args-gradle-plugin:$navigationVersion"
	classpath "androidx.navigation:navigation-safe-args-gradle-plugin:$navigationVersion"

* build.gradle (app):

1. Add plugins: (at the top, together with the other plugins)

	apply plugin: 'kotlin-kapt'
	apply plugin: 'androidx.navigation.safeargs'

2. In _defaultConfig_ section, add the option to use vector drawables:

    vectorDrawables.useSupportLibrary = true

3. Add buildFeatures section for data binding:

    buildFeatures {
        dataBinding true
    }

4. Add stuff to _dependencies_ section:
	(versions may be changed in the future)

    // Support libraries
    implementation "androidx.fragment:fragment:1.2.5"

    // Android KTX
    implementation 'androidx.core:core-ktx:1.3.1'

    // Room and Lifecycle dependencies
    implementation "androidx.room:room-runtime:$room_version"
    implementation 'androidx.legacy:legacy-support-v4:1.0.0'
    kapt "androidx.room:room-compiler:$room_version"
    implementation "androidx.lifecycle:lifecycle-extensions:2.2.0"
    implementation "androidx.lifecycle:lifecycle-viewmodel-ktx:2.2.0"

    // Kotlin Extensions and Coroutines support for Room
    implementation "androidx.room:room-ktx:$room_version"

    // Coroutines
    implementation "org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutine_version"
    implementation "org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutine_version"

    // Navigation
    implementation "android.arch.navigation:navigation-fragment-ktx:$navigationVersion"
    implementation "android.arch.navigation:navigation-ui-ktx:$navigationVersion"



# Defining database entities

* https://developer.android.com/training/data-storage/room


In Kotlin, it's possible to define all data classes in the same file!

The table column names are the same as the class field names, unless specified explicitly in _@ColumnInfo_ annotation.

It's possible to specify indices to speed up search, and also to add unique indices. It's done in the @Entity annotation. For example:

	@Entity(tableName = "quests",
	    indices = arrayOf(Index(value = ["category"], name = "category"),
	                      Index(value = ["name"], name = "name", unique = true)))

Here, _name_ is the index name in the database (can be omitted), and the _value_ is the collection of the database columns used in the index - it's an array because indices can be composite.


The time/date columns have to be stored as Long.

# Defining DAO

Use annotations @Insert, @Update and @Delete for all queries which use primary keys to find the data to process. Otherwise, use @Query annotation for a general-purpose query.

All the functions should start with _suspend_ keyword unless they return LiveData. (I suppose.)

# Creating database

You can't call your class simply _Database_, because there's a conflict with a standard Android class with the same name. Otherwise, there's the template which can be copied as-is from RecyclerViewClickHandler, just change the names.

An example of prepopulating the database on creation from a hardcoded list: 
https://gist.github.com/florina-muntenescu/697e543652b03d3d2a06703f5d6b44b5

Apparently the database is not created automatically, unless the code refers to it.

# Adding navigation (start)

1. Create _res/navigation_ and empty _navigation.xml_ via Android Studio

2. Replace the activity_main.xml with a layout for fragments (copy/paste from RVCH). 

3. Now you can use navigation Design Editor to add fragments. 

See also https://codelabs.developers.google.com/codelabs/kotlin-android-training-add-navigation

# Adding view models

1. Create QuestsViewModel and then QuestsViewModelFactory like in RVCH, very simple - for the livedata, only add the text of all the quests formatted with formatQuests in Util.kt (temporarily!)

2. Convert fragment_quests.xml for data binding (can do via Android Studio), add the QuestsViewModel

3. Update QuestsFragment to use data binding and the view model. (No RecycleView and adapter yet, also no navigation)

# Dump the database

After the database contents are displayed, it's possible to access it through adb and dump it!

see https://developer.android.com/training/data-storage/room/testing-db

   adb -s emulator-5554 shell (for the emulator)
   sqlite3 data/data/org.catness.quest/databases/quests_database
   sqlite> .schema
   sqlite> .dump



