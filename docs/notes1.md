# Creating recycler view adapter

1. fragment_quests.xml : change for recycler view, give it an id (quest_list)
   Note: it must include app:layoutManager! for example ="androidx.recyclerview.widget.LinearLayoutManager" 
   or create layout manager dynamically (like Grid in the example).



2. create new file: QuestsAdapter.kt. Don't create the main class QuestsAdapter yet. Meanwhile:

3. add item_view types constants

4. create sealed class DataItem, which provides id from JournalItem and QuestItem

5. create class QuestDiffCallback extending DiffUtil

6. create classes QuestListener and JournalListener, using quest and journal params respectively



7. create layouts for list_item_journal and list_item_quest. They have to data-bind to their corresponding data entities 
Quest and Journal (e.g. org.catness.quest.database.Quest). The height of constraint layout is wrap_content.

8. Now create the main class QuestsAdapter (with 2 clickListener params - type QuestListener and JournalListener)

9. add function addHeaderAndSubmit, which takes 2 lists (quests and journals) and merges into 1 list of DataItem

10. create inner class JournalViewHolder (like in the RecyclerView example), and the same thing for QuestViewHolder. 
The _binding_ parameter name is same as the layout name but converted to camel case and with "Binding" suffix.


11. add getItemViewType like in the example

12. implement onCreateViewHolder and onBindViewHolder

13. in QuestsFragment.kt, create the adapter (with both callbacks) and add 2 observers for the time being, one for quests and one for journals (pass the other parameter as livedata value to adapter.addHeaderAndSubmit)



14. Later on: it's possible to observe both livedata sources with MediatorLiveData. See example at:
  * https://old.black/2020/04/20/using-mediatorlivedata-to-get-data-from-two-tables-at-once/
