# Adding journal detail fragment and navigation

1. In navigation design editor, create a new fragment (with empty template): JournalDetailFragment

2. Create a package (journaldetail), move the .kt file there

3. In navigation design editor, click on JournalDetailFragment and add an argument to it: journalKey (Long). No default value, otherwise it will not compile!

4. Create JournalDetailViewModel (in the package). It needs journal variable as LiveData. And it has 1 argument - journalKey. (the ID)

5. Create JournalDetailViewModelFactory. 

6. Convert fragment_journal_detail.xml for data binding. Add variable journalDetailViewModel.

7. Edit fragment_journal_detail layout to show some data about the journal. Make sure to convert the time/date Long variables to strings with Transformations.map. (It works on single variables too, not only lists.)

8. Edit JournalDetailFragment. (onCreateView) like in the example. Note that _JournalDetailFragmentArgs_ is marked as error, but it will be ok after clean/rebuild.

9. Edit QuestsViewModel - add the navigation stuff _navigateToJournalDetail_ :
   (some of the underscores here are not part of the code, just to avoid Sublime Text marking it as error)

   a) the private val _ _navigateToJournalDetail = MutableLiveData<Long>()_
   b) getter for it: 
      	val navigateToJournalDetail
        get() = _ _navigateToJournalDetail_
   c) a callback to reset it:
    fun onJournalDetailNavigated() {
        _ _navigateToJournalDetail.value = null_
    }      
    d) the actual onClicked callback:
    fun onJournalClicked(id: Long) {
     _  _navigateToJournalDetail.value = id_
    }

10. Edit QuestsFragment - add the navigation stuff
    a) add the reference to the onClicked callback to the adapter creation (the JournalListener lambda)
    b) add the observer for navigateToSleepDetail, calling the navController stuff

