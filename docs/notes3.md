# Adding quest detail fragment and navigation

1. In navigation design editor, create a new fragment (with empty template): QuestDetailFragment. Add navigation from questsFragment. Add questId (Long) argument to QuestDetailFragment.

2. Create questdetail package, move QuestDetailFragment to it

3. Create QuestDetailViewModel and QuestDetailViewModelFactory. 

4. Convert fragment_quest_detail.xml for data binding. Add variable questDetailViewModel. Add all the textviews and the COMPLETED button.

5. Edit QuestDetailFragment. (onCreateView) like in the example. 

Note: to get rid of compiler warning about fragment arguments, it's possible to change the auto generated code to:

  val arguments = arguments?.let { QuestDetailFragmentArgs.fromBundle(it) }

and then:

  val viewModelFactory = QuestDetailViewModelFactory(requireArguments().getLong("questKey"), dataSource)

6. Edit QuestsViewModel - add the navigation stuff _navigateToQuestDetail_  (same like for the Journal)

7. Edit QuestsFragment - add the navigation stuff for Quest (ditto)

8. In navigation design editor, add navigation from QuestDetailViewModel back to QuestsFragment. (No arguments.)

9. Edit QuestDetailViewModel - add the navigation stuff with _navigateToQuestsFragment_ (the var is boolean because there are no arguments to send). Add also onCompleted() callback which sets this variable to true (so far no more actions). 

10. Add the reference to onCompleted() callback to the _completed_ button in fragment_quest_detail.xml
    
    android:onClick="@{() -> questDetailViewModel.onCompleted()}"

11. In QuestDetailFragment, add the observer for _navigateToQuestDetail_ to navigate back to QuestsFragment.

12. Now add the actual action. In QuestDetailViewModel, in onCompleted, add viewModelScope.launch {} with all the stuff to do upon completing the quest - mark it as completed, pick a new one, insert a new journal entry.
