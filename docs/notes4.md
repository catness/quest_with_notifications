# Parameterized strings example 


 android:text="@{@string/reward_message(journalDetailViewModel.journal.reward)}"

# SQLite notes

* Names of the indices throughout the whole database must be distinct! If 2 tables have the field "name", and both need them as an index, they can't be both called "name".

* Upon changing the database structure, it's not necessary to change the version - just delete the app from all the devices and reinstall after update.

# Menu

* Is quite straightforward as in AndroidTriviaNavigation https://codelabs.developers.google.com/codelabs/kotlin-android-training-add-navigation/#8 . The fragments used in the menu don't have to be connected to the other fragments in navigation editor - the navigation is done through the menu. (Assuming that the meni item id is the same as the fragment id.)

# Up button

* From the same lab - it requires to add a couple of things to the main activity.

# Fragment titles

* Make sure to change the "label" in the navigation.xml to the required title of the fragment.

* When the label doesn't work, it's possible to set the title manually in onCreateView. For example, the inventory fragment:

  (activity as MainActivity).supportActionBar?.title = getString(R.string.inventory)

# Customize buttons

* Define the custom style with parent="Widget.AppCompat.Button.Colored" . (See the example.) In the layout, the keyword is not _style_ but _android:theme_!

